# Introduction

Democratic institutions need tending, modern government institutions have failed to adapt and react to the pace of change of the current time. Government's must use modern devop's toolkit to effectively respond to pressures of change and enact proper solutions.

Structured Planning seeks to resolve some latencies by creating inclusive platforms which function as "front-ends" to pressing issues of the day. This inclusive procurement process will bring more brain power into the problem solving realm and will allow all concerns to be handled appropriately. The discussions which used to occur only during meeting times convenient for those setting the schedule will be able to persist as data on the internet. In this manner, we will be able to solve the government problems of today and tomorrow in a coordinated, inclusive fashion. Structured planning can be viewed as a process like agile development, except it favors inclusivity over pace. It is the ideal strategy for building government programs.

Structured Planning is a design philosophy pioneered by Professor Charles Owens of IIT School of Design. It formalizes certain features to ensure that all voices are heard, all concerns are raised, and access to necessary information is ubiquitous. Governments, Corporations, and individuals can use Structured Planning to create a well negotiated, fair solution to existing problems. Some may find structured planning useful in dAPP development as well, especially if the creators prioritize decentralization.

# App Design

## Tech Stack
  Go or Haskell backend
  Elm frontend w/typescript?

## User Authentication
  [support for anonymity, but avoid trolls](https://cs.stanford.edu/people/eroberts/cs181/projects/anonymous-computing/history/law.php3)

## Build
  Mostly filling out forms and handling forms:
          * NLP? Unsupervised learning to Categorise issues? Maybe it's someone's job to read and catagorize instead.
	  * Voting may be better feature to raise issues although uniqueness from minority instances who have small voice are unlikely to be heard through voting. (mob-rule/reddit problem)
	     * If voting, how to couple annonymity with identity without exposure (zk-snarks?) (search for papers on annonymity)

  Chan-like interface for meme generation, fork voat? Both?
    * Chan 4 memes, voat's for visibility?
    * How are moderators selected?
  (weighted?) Voting (i.e. Experts > non-experts) | (vote on weighted voting?) 
  
  Karma-integration for reputation management?
 

## Flow:
 List of Issues properly categorized by domain (justice, healthcare, energy, etc) + Ability to create new issues
 Issues need to be handled

 Charter's are proposed solutions to Issues
 Rest should follow Charles Owen's paper
 
 The goal is to create an environment where expert opinions rise to the top, yet citizen concerns are voiced, considered, and accounted for.

 If anybody has any other paper's which would be useful towards this design process, please create an issue in the side bar.
 If you see any issue's with this approach, feel free to create an issue in the side bar.

# Getting Started

## Unix know how
 Mac's work as they are based on UNIX, if you have a Windows machine and seek to enter into the world of Freedom but do not know where to start, these guys would love to help you: [Windy City Linux User's Group](https://www.meetup.com/wclug-org/)

### Command-line

#### Beginner
  [command line tutorial](https://www.learnenough.com/command-line-tutorial)  

#### Intermediate

  [art of command line](https://github.com/jlevy/the-art-of-command-line)

### Text-Editor

[text editor tutorial (vim)](https://www.learnenough.com/text-editor-tutorial) 
[vim-adventure (game)](https://vim-adventures.com/)
[markdown tutorial](https://www.markdowntutorial.com/)


### Git

 [git-tutorial](https://www.learnenough.com/git-tutorial)


There may be more effective ways to go about contributing to this project, however good documentation makes useful software useable.


# Inspirations:
Feel free to make add any suggestions

## Design
 [Dubberly](http://www.dubberly.com/articles)


# Todo 
Rewrite in the form of Structured Planning to show an example. Will make platform much less opinionated.

